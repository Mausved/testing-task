package main

import (
	"encoding/json"
	"fmt"
	"log"      // пакет для логирования
	"net/http" // пакет для поддержки HTTP протокола
)

type Response struct {
	Status  int
	Message string
}

const (
	defaultErrorMessage     = "something went wrong..."
	defaultGetMessage       = "ok"
	defaultPostMessage      = "Updated"
	defaultJsonErrorMessage = "error due to working with JSON..."
)

func HomeRouterHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm() //анализ аргументов
	var response Response
	if err != nil {
		response.Message = defaultErrorMessage
		response.Status = http.StatusBadRequest
	} else {
		if r.Method == http.MethodGet {
			response.Message = defaultGetMessage
		} else if r.Method == http.MethodPost {
			response.Message = defaultPostMessage
		} else {
			response.Message = "other method"
		}
		response.Status = http.StatusOK
	}
	result, err := json.Marshal(response)
	if err != nil {
		fmt.Println(defaultJsonErrorMessage)
		fmt.Println(w, defaultErrorMessage)
	} else {
		fmt.Println("json is: ", string(result))
		fmt.Fprintf(w, string(result)) // отправляем данные на клиентскую сторону
	}
}

func main() {
	http.HandleFunc("/hello", HomeRouterHandler) // установим роутер
	err := http.ListenAndServe(":5000", nil)     // задаем слушать порт
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
